import React from 'react';

export type ButtonMouseEvent = React.MouseEvent<HTMLButtonElement>;
